#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const int BUFFSIZE = 200;

void* lerArquivo(char* nomeArquivo) ;

int main(int argc, char *argv[]) {
	if(argc == 1){
		perror("Os nomes dos arquivos não foram informados");
		exit(EXIT_FAILURE);
	}

	printf("Totais de linhas em cada arquivo\n");
	int i;
	for (i = 1; i < argc; ++i) {
		lerArquivo(argv[i]);
	}
}

void* lerArquivo (char* nomeArquivo){
	char buffer[BUFFSIZE];
	char* p = buffer;
	long count = 1;

	int n = 1;
	int fd = open(nomeArquivo, O_RDONLY);

	while (n > 0) {
		n = read(fd, // descrito do arquivo
				p, //endereço do buffer que receberá os dados
				BUFFSIZE //capacidade maxima do buffer
				);
		int i;
		for (i=0; i < n; i++) {
			if(buffer[i] == '\n') ++count;
		}

		if(n == -1){
			count=-1;
		}

	}

	printf("%s : %ld\n", nomeArquivo, count);
	close(fd);
	return EXIT_SUCCESS;

}
