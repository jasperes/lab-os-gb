#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

const int BUFFSIZE = 200;

void* ler(void* param) ;

int main(void) {
	//TODO Pegar quantidade de arquivos
	int NTHREADS = 3;
	int i;
	pthread_t idthread[NTHREADS];
	char* arquivo = "ABC";

	for (i=0; i<NTHREADS; i++) {
		printf("Criando a thread %i \n", i);
		int err = pthread_create(
				&idthread[i], // var. para receber ID da thread criada
				NULL, // &att, // atributos
				&ler, // função ponto-de-entrada da thread
				arquivo// o parametro da thread
		);


		if (err) {
			perror("pthread_create() falhou");
			exit(EXIT_FAILURE);
		}
	}

	pthread_exit(NULL);
}


void* ler(void *param) {
	char* arquivo  = (char*) param;
	printf("%s\n", arquivo);
	return EXIT_SUCCESS;

}

void* bkp (void* param){
	char buffer[BUFFSIZE];

	int n;
	char* p = buffer;
	long count = 1;

	n = 1;
	int fd = open("/home/ahevdrich/workspace/contabiliza/src/TESTE", O_RDONLY);

	while (n > 0) {
		n = read(fd, // descrito do arquivo
				p, //endereço do buffer que receberá os dados
				BUFFSIZE //capacidade maxima do buffer
				);

		int i;
		for (i=0; i < n; i++) {
			if(buffer[i] == '\n') count ++;
		}
	}

	printf("%ld\n", count);
	close(fd);
	return EXIT_SUCCESS;

}
