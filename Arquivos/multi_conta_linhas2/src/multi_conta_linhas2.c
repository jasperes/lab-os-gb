#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const int BUFFSIZE = 200;

void* criarThreads(int numThreads, char* nomesArquivos[]);
void* lerArquivo(void* param);

int main(int numeroParametros, char *parametros[]) {

	// Verifica se todos os parametros foram recebidos
	if (numeroParametros == 1) {
		perror("Os nomes dos arquivos não foram informados");
		exit(EXIT_FAILURE);
	}

	printf("Totais de linhas em cada arquivo\n");

	// Chama função que irá criar as threads
	criarThreads(numeroParametros, parametros);

	return EXIT_SUCCESS;
}

void* criarThreads(int numeroParametros, char* nomesArquivos[]) {
	//Subtraindo um da lista de paramtro rebecidos, pois o primeiro parametro eh o nome do programa a ser executado
	int NTHREADS = numeroParametros - 1;

	// Cria um array de thread com o numero de parametros passado
	pthread_t idthread[NTHREADS];

	int i;
	// cria um loop para executar cada uma das threads
	// criada no array
	for (i = 1; i <= NTHREADS; i++) {
		// Pega o arquivo da lista de arquivos recebido
		// Nos parametros, correspondente ao item atual
		char* arquivo = nomesArquivos[i];
		// Cria a thread e armazena valor de finalização de sua execução em 'erro'
		int erro = pthread_create(&idthread[i], // var. para receber ID da thread criada
				NULL, // &att, // atributos
				&lerArquivo, // funcao ponto-de-entrada da thread
				arquivo // o parametro da thread
				);

		// Caso tenha ocorrido algum erro, finaliza execução do programa
		if (erro) {
			perror("Ocorreu uma falha na criação da thread");
			exit(EXIT_FAILURE);
		}
	}

	// Finaliza execução da thread
	pthread_exit(NULL);
}

void* lerArquivo(void *param) {
	// Armazena o nome do arquivo à ler recebido por parametro
	char* nomeArquivo = (char*) param;

	// Cria o buffer, que receberá o conteúdo do arquivo
	char buffer[BUFFSIZE];
	char* enderecoBuffer = buffer;
	// contador de número de linhas lido do arquivo
	long numeroLinhas = 1;

	// inteiro para verificar a quantidade de bytes lido do arquivo
	int bytesLidos = 1;
	// Abri o arquivo como leitura
	int descritorArquivo = open(nomeArquivo, O_RDONLY);

	// Cria um loop para ler o arquivo, enquanto o contador não chegou à zero
	while (bytesLidos > 0) {
		bytesLidos = read(descritorArquivo, // descrito do arquivo
				enderecoBuffer, //endereço do buffer que receberá os dados
				BUFFSIZE //capacidade maxima do buffer
				);
		int i;
		// Percorre a quantidade de bytes lidos e...
		for (i = 0; i < bytesLidos; i++) {
			// ...caso o byte corresponda a um caractere de nova linha
			if (buffer[i] == '\n')
				// Adiciona +1 ao contador de linhas lido
				++numeroLinhas;
		}

		//Se ao tentar ler um arquivo, o mesmo não for encontrado, atribui menos -1 no contador de linhas
		if (bytesLidos == -1) {
			numeroLinhas = -1;
		}
	}

	// Por fim, escreve em tela o nome do arquivo lido e a quantidade de linhas no mesmo
	printf("%s : %ld\n", nomeArquivo, numeroLinhas);
	// Fechando o arquivo
	close(descritorArquivo);

	return EXIT_SUCCESS;
}
