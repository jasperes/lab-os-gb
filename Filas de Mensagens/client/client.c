#include <fcntl.h>
#include <mqueue.h>
#include <stddef.h>
#include <stdlib.h>

void* send(const char *senha_str, int prio);
void* post();
void* get();

int main(void) {

	post();
	get();

	exit(EXIT_SUCCESS);
}

void* post() {
	int prio = 1;
	const char *senha_str = "user\t123\tname\t";

	send(senha_str, prio);

	return EXIT_SUCCESS;
}

void* get() {
	int prio = 0;
	const char *senha_str = "user\t123\t/outrafila";

	send(senha_str, prio);

	return EXIT_SUCCESS;
}

void* send(const char *senha_str, int prio) {
	const char *app_name = "/cofredesenhas";
	mqd_t mqd = mq_open(app_name, O_WRONLY);
	size_t str_len = strlen(senha_str);
	mq_send(mqd, senha_str, str_len, prio);

	return EXIT_SUCCESS;
}
