#include <fcntl.h>
#include <mqueue.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void* post(char *senha_str);
void* get(char *senha_str, const char *senha_mestre);
void* send(char *fila, char *usuario, char *senha);
int chave(char *usuario);

struct cofre_senha {
	const char *usuario;
	const char *senha;
	const char *nome;
};

int slen = 0;
const int smax = 10;

struct cofre_senha senhas[10];

int main(int argc, char *argv[]) {

	const char *app_name = "/cofredesenhas";
	const char *senha_mestre = argv[1];

	size_t msg_size = 1024;
	char *buf[msg_size +1];
	const int prio;

	struct mq_attr attr;
	attr.mq_flags = 0;
	attr.mq_maxmsg = 10;
	attr.mq_msgsize = msg_size;
	attr.mq_curmsgs = 0;

	if (senha_mestre == '\0') {
		exit(EXIT_FAILURE);
	}

	mqd_t mqd = mq_open(app_name, O_CREAT | O_RDONLY, 0644, &attr);

	if (mqd < 0) {
		perror("mq_open error");
		exit(EXIT_FAILURE);
	}

	while (1) {
		if (mq_receive(mqd, buf, msg_size, &prio) < 0) {
			perror("mq_receive error");
		}

		buf[msg_size] = '\0';

		switch(prio) {
		case 1:
			post(buf);
			break;
		case 0:
			get(buf, senha_mestre);
			break;
		default:
			break;
		}

	}

	exit(EXIT_SUCCESS);
}

void* post(char *senha_str) {

	if (slen == smax) {
		write(STDERR_FILENO, "Chaveiro lotado!", 16);
		return EXIT_FAILURE;
	}

	char *filter;
	char *usuario;
	char *senha;
	char *nome;

	strtok_r(senha_str, "\t", &filter);
	usuario = senha_str;
	strtok_r(filter, "\t", &nome);
	senha = filter;
	strtok_r(nome, "\t", &filter);

	senhas[slen].usuario = usuario;
	senhas[slen].senha = senha;
	senhas[slen].nome = nome;

	slen++;

	return EXIT_SUCCESS;
}

void* get(char *senha_str, const char *senha_mestre) {

	int fd[2];
	int BUFFSIZE = 32;
	char buffer[BUFFSIZE];
	char *buf = buffer;
	int n;

	char *filter;
	char *usuario;
	char *senha;
	char *fila;

	strtok_r(senha_str, "\t", &filter);
	usuario = senha_str;
	strtok_r(filter, "\t", &senha_str);
	senha = filter;
	strtok_r(fila, "\t", &filter);

	if (pipe(fd) < 0) {
		perror("pipe()");
		exit(EXIT_FAILURE);
	}

	switch (fork()) {
	case -1:
		perror("fork()");
		exit(EXIT_FAILURE);

	case 0:
		close(fd[0]);
		dup2(fd[1], STDOUT_FILENO);
		execlp("zenity", "zenity", "--password", senha_str, NULL);
		break;

	default:
		close(fd[1]);
		while((n = read(fd[0], buf, BUFFSIZE)) < 0);

		if (buf[n-1] == '\n'){
			buf[n-1] = '\0';
		} else {
			buf[n] = '\0';
		}

		if (strcmp(buf, senha_mestre) == 0) {
			send(fila, usuario, senha);
		} else {
			return EXIT_FAILURE;
		}

		break;
	}

	return EXIT_SUCCESS;

}

void* send(char *fila, char *usuario, char *senha) {
	int n;
	if ((n = chave(usuario)) < 0) {
		return EXIT_FAILURE;
	}

	struct cofre_senha s = senhas[n];
	int i = strlen(s.usuario) + strlen(s.senha) + strlen(s.nome) + 4;
	char to_send[i];

	strcpy(to_send, "");
	strcat(to_send, s.usuario);
	strcat(to_send, "\t");
	strcat(to_send, s.senha);
	strcat(to_send, "\t");
	strcat(to_send, s.nome);
	strcat(to_send, "\t");

	to_send[i] = '\0';

	mqd_t mqd = mq_open(fila, O_WRONLY);
	size_t str_len = strlen(to_send);
	mq_send(mqd, to_send, str_len, 0);

	return EXIT_SUCCESS;
}

int chave(char *usuario) {
	int i;
	for (i=0; i<=smax; i++) {
		if (strcmp(senhas[i].usuario, usuario) == 0) {
			return i;
		}
	}

	return EXIT_FAILURE;
}
