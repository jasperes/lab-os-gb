#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void* cat(const char *file) ;
void* gzip_cat(const char *file) ;

int main(int argc, char *argv[]) {

	const char *file = argv[1];

	char* zip_extencao = ".gz";
	int zip_size = 3;

	size_t size = strlen(argv[1]);
	const char *extencao = &file[size-zip_size];
	int isZip = strncmp(extencao, zip_extencao, zip_size);

	if (isZip != 0) {
		cat(file);
	} else {
		gzip_cat(file);
	}

	return EXIT_SUCCESS;
}

void* cat(const char *file) {
	char buf;
	int fd = open(file, O_RDONLY);
	while (read(fd, &buf, 1) > 0)
		write(STDOUT_FILENO, &buf, 1);
}

void* gzip_cat(const char *file) {

	int fd[2];
	char buf;

	if (pipe(fd) < 0) {
		perror("pipe()");
		exit(EXIT_FAILURE);
	}

	switch (fork()) {
		case -1:
			perror("fork()");
			exit(EXIT_FAILURE);

		case 0:
			close(fd[0]);
			dup2(fd[1], STDOUT_FILENO);
			execlp("gzip", "gzip", "-cd", file, NULL);
			perror("exec gzip error");
			break;

		default:
			close(fd[1]);
			while (read(fd[0], &buf, 1) > 0)
				write(STDOUT_FILENO, &buf, 1);
			break;
		}
}
