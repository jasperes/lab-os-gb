#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const int BUFFSIZE = 200;

void* run_gc(char *file);
void* cat(char *file) ;
void* gzip_cat(char *file) ;

int main(int argc, char *argv[]) {
	
	const char *fifo = argv[1];

	while(1) {

		char buffer[BUFFSIZE];
		char *buf = buffer;
		int n;
		int fd = open(fifo, O_RDONLY);

		while((n = read(fd, buf, BUFFSIZE)) < 0);

		if (buf[n-1] == '\n'){
			buf[n-1] = '\0';
		} else {
			buf[n] = '\0';
		}

		run_gc(buf);
	}

	exit(EXIT_SUCCESS);
}

void* run_gc(char *file) {

	char* zip_extencao = ".gz";
	int zip_size = 3;

	size_t size = strlen(file);
	const char *extencao = &file[size-zip_size];
	int isZip = strncmp(extencao, zip_extencao, zip_size);

	if (isZip != 0) {
		cat(file);
	} else {
		gzip_cat(file);
	}
}

void* cat(char *file) {
	char buf;
	int fd = open(file, O_RDONLY);
	while (read(fd, &buf, 1) > 0)
		write(STDOUT_FILENO, &buf, 1);
}

void* gzip_cat(char *file) {

	int fd[2];
	char buf;

	if (pipe(fd) < 0) {
		perror("pipe()");
		exit(EXIT_FAILURE);
	}

	switch (fork()) {
	case -1:
		perror("fork()");
		exit(EXIT_FAILURE);

	case 0:
		close(fd[0]);
		dup2(fd[1], STDOUT_FILENO);
		execlp("gzip", "gzip", "-cd", file, NULL);
		perror("exec gzip error");
		break;

	default:
		close(fd[1]);
		while (read(fd[0], &buf, 1) > 0)
			write(STDOUT_FILENO, &buf, 1);
		break;
	}
	
}
